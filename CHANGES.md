### Changelog

#### 1.2.0 [11/13/2014]

* Added support for client info tag double curly bracket syntax (E.G. ```{{key}}```)
* Added support for shortcodes
* Spacing now defaults to 0.5em instead of 0

#### 1.1.2 [09/10/2014]

* Adjusted order of fields a bit

#### 1.1.1 [08/18/2014]

* This update requires DMS Skeleton theme version 0.6.1 or newer.
* Spacing options added to allow you to set the margin between heading and subheading as well as the bottom margin of the group of headings or a standalone heading. The spacing also scales with the font sizes.

#### 1.1.0

* This update requires SMS version 1.0.6 or newer and DMS Skeleton theme version 0.6.0 or newer.
* Text updates in realtime now!
* Default text only displays in the editor now.
* Added viewport animation options. Animations are triggered when the heading becomes visible in the viewport.
* Revamped indicators
* Added letter spacing
* Added line-height
* Fixed issue that caused alignment settings to be overwritten by the band options by making the CSS property ```!important```
* Fixed italic option by making the CSS property ```!important```
* Fixed heading spacing above and below

#### 1.0.2

 * Fixed issue that's breaking compilation of section less
 * Added examples of how to add custom less variables and less output

#### 1.0.1

 * Updated thumbnail
 
#### 1.0.0
 
 * Initial release
